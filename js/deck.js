const mongoose = require("mongoose")

const UserSchema = new mongoose.Schema(
	{
		deckId: { type: Int16Array, required: true, unique: true},
		deckName: {type:String, required: true, unique: true},
		word: { type: Array, required: true },
        translation: {type: Array, required: true}
	},
	{ collection: 'deck' }
)

const model = mongoose.model('DeckSchema', DeckSchema)

module.exports = model