# QuizApp von Julia & Dean
## Informationen
**Gitlab-Link:** https://gitlab.com/julia.huettenmoser/quizapp

### MongoDB
**Username:** julia

**Passwort:** 1234
## Auftragsbeschreibung
### Ziele
- [X] Lehrpersonen und Schüler können sich einloggen bzw. registrieren 
- [ ] Lehrpersonen können Deck für Lernende erstellen
- [ ] Lernenden sollen ein Wort sehen und die Übersetzung eintragen können
- [ ] Falsche Antworten sollen wiederholen werden um nochmal eine Antwort an zu geben

### Auftragsanlyse
1. User muss bei Log in angeben können ob er Lehrer oder Schüler ist
2. Lehrpersonen müssen ein Interface mit Decknamen und eine Liste von Wörtern und deren Übersetzungen haben. Die Lernenden dieses Deck mit einer Searchbar suchen können
3. Schüler werden auf einen Screen mit einem Wort und einem Eingabefeld geleitet, wo sie die Übersetzung eingeben müssen
4. Bei korrekter Übersetzung wird ein grüner Hacken angezeigt, wobei bei einer falscher Übersetzung wird ein rotes Kreuz abgebildet und die wörter werden in einem Array gespeichert, nachdem der User alle Wörter beantwortet hat, werden die Wörter im Array nochmals abgefragt, gleiches System wie zuvor.

### User-stories
#### Lehrer
Ich möchte...
- eine Übersicht von allen meinen Decks haben
- meine Decks überarbeiten können
#### Schüler
Ich möchte...
- die Decks meiner Lehrer schnell und leicht finden
- meine Fehler einsehen können um mich zu verbessern
#### Lösungen
- Lehrer Seite mit all den Decks des Lehrers
- Patch Request mit Deck-ID
- Suchleiste wo man nach Deckname oder Lehrer suchen kann
- nach falscher Anwort die korrekte Lösung abbilden

### Requirements Zusammengefasst
- Lehrer und Schüler können sich einloggen
- Lehrer können Decks erstellen
- Decks sind auf Lehrer-Seite einsehbar
- Suchleiste für Lehrer oder Decks
- Schüler können bei Deck Übersetzungen für Wörter angeben
- Bei falscher Antwort wird korrekte Lösung angezeigt und am Ende nochmals abgefragt
- Lehrer können Decks anpassen 

![Drawn Wireframe](img/sketch.jpg)
---


## Umsetzung
### Registrierung
Für die Registrierung haben wir uns für lediglich 3 Faktoren fokussiert: Username, Passwort und ob der User ein Lehrer ist oder nicht. Mit einem Form hat man zwei Eingabefelder und eine Checkbox für den Lehrer (wir haben diesen als Boolean abgespeichert)

![Wireframe Register](img/register.png)

Wie sie im obrigen Bild sehen haben wir im Wireframe noch ein wiederholtes Passwort geplant gehabt, jedoch haben wir dies aus Überflüssigkeit weggenommen.
### Login
Beim Login werden nur Username und Passwort abgefragt, wobei nach der Eingabe gecheckt wird, ob dieser Account ein Lehrer ist oder nicht und je nach dem auf die passende Homepage weitergeleitet wird.
### Homepage
 Die Lehrer-Homepage hat Optionen, um ein Deck zu erstellen, wobei die Schüler auf ihrer Homepage nach dem Deck suchen können, sie abzurufen und damit zu lernen.