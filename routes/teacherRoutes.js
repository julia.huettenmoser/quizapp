const express = require('express');


const router = express.Router();

router.param('id', teacherController.checkID);

router
  .route('/')
  .get(teacherController.getAllTeachers)
  .post(teacherController.createTeacher);

router
  .route('/:id')
  .get(teacherController.getTeacher)
  .patch(teacherController.updateTeacher)
  .delete(teacherController.deleteTeacher);

module.exports = router;