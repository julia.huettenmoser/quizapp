const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
// const User = require('./model/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
// const { config } = require('dotenv');

const JWT_SECRET = "secret";

var MongoClient = require('mongodb').MongoClient;

var uri = "mongodb://julia:1234@ac-m6ew5oa-shard-00-00.i8bhgco.mongodb.net:27017,ac-m6ew5oa-shard-00-01.i8bhgco.mongodb.net:27017,ac-m6ew5oa-shard-00-02.i8bhgco.mongodb.net:27017/?ssl=true&replicaSet=atlas-nlvw8z-shard-0&authSource=admin&retryWrites=true&w=majority";
MongoClient.connect(uri, function(err, client) {
  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
  client.close();
});



// User schema

const quizapp = new mongoose.Schema({
	"username": { type: String, required: true, unique: true },
	"password": { type: String, required: true },
	"teacher": { type: Boolean, required: true }
});

// const UserModel = mongoose.model('User', quizapp);

const app = express();
app.use('/', express.static(path.join(__dirname, 'static')));
app.use(bodyParser.json());

//register

app.get("/register", (req, res)=>{
    res.render("views/register.ejs")
});

app.post('/api/register', async (req, res) => {
	const user = new UserModel({
		username: req.body.username,
		password: req.body.password,
		teacher: req.body.teacher
	});

	const salt = await bcrypt.genSalt(10);
	user.password = await bcrypt.hash(user.password, salt);

	try {
		await user.save();
		res.send('Signup successful!');
	} catch (error) {
		res.status(500).send(error);
	}
});

//login

app.get("/login", (req, res)=>{
    res.render("login.ejs")
});

app.post('/login', async (req, res) => {
	try {
		const user = await UserModel.findOne({ username: req.body.username });
		const isPasswordValid = await bcrypt.compare(req.body.password, user.password);
		if (!user || !isPasswordValid) {
			return res.status(401).send('Incorrect username or password');
		} else{
			const token = jwt.sign({ _id: user._id }, JWT_SECRET);
			res.header('Authorization', `Bearer ${token}`).send('Login successful!');
			if(req.body.teacher == true){
				res.redirect("/api/teacherIndex");
			} else{
				res.redirect("/api/index");
			}
		}
	} catch (error) {
		res.status(500).send(error);
	}
});

//index

app.get("/index", (req, res)=>{
    res.render("index.ejs")
});

//server

app.listen(9999, () => {
	console.log('Server up at 9999');
});