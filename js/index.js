const express = require('express');
const morgan = require('morgan');

const tourRouter = require('./routes/tourRoutes');
const userRouter = require('./routes/userRoutes');

var port = process.env.PORT || 3000;

mongoose.connect('mongodb+srv://julia:1234@cluster0.1cx85fb.mongodb.net/test', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true
})
const app = express();